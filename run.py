#!/usr/bin/python
VERSION = '0.1.1'

import imp
from bson.objectid import ObjectId
from pymongo import MongoClient
import datetime
from sys import argv, exit, stdout
import config
import time

#       args parsing
filename = None
args = argv[1:]
if len(args):

        if args[0] in ['--name', '-n']:
                filename = args[1]
        elif args[0] in ['--help', '-h']:
                print '[INFO] Use parameter -n or --name for scraper selection'
                exit(1)
        elif args[0] in ['--version', '-v']:
                print '[INFO] Version {ver}'.format(ver=VERSION)
                exit(1)
        else:
                print '[WARNING] Wrong first parameter! \n[WARNING] your first parameter should be "-n" or "--name"'
                exit(1)
                
else:
        print '[INFO] No parameters! Your first parameter should be "-n" or "--name"'
        exit(1)
#       args parsing

#       file load
greating = config.GREATING.format(filename=filename)
print greating

use_scraper = config.DEV_SCRAPER_PATH
scraper_path = use_scraper.format(filename=filename)

try:
        scraper_name = imp.load_source('indonesia', scraper_path)
except IOError:
        print '[WARNING] Wrong filename! \n[WARNING] your input: {f} , be more accurat'.format(f=filename)
        exit(1)
#       file load

#       setup mongo
client = MongoClient(config.MONGO_PATH)
pep_persons = client.pep_persons
persons = pep_persons.persons
#       setup mongo

person_counter = 0
for count, person in enumerate(scraper_name.main(write_to_mongo=True)):
        updated_at = {'updated_at': datetime.datetime.utcnow(), '_id': person['name']}
        person.update(updated_at)
        update = {
                '$set': {
                        'source': filename,
                        'count_number': count
                        },
                '$setOnInsert': {'created_at': datetime.datetime.utcnow()}
        }
        persons.update(person, update, upsert=True)
        person_counter += 1
        
print '='*50
print 'upsert finished'
print 'upserted {count} persons'.format(count=person_counter)
print '='*50


        
        

        
