# /usr/bin/python -tt
# coding=utf-8
import json
from string import ascii_lowercase
from bs4 import BeautifulSoup
from utils import create_entity, create_id
import helpers

POL_REG = 'political_region'
POL_PARTY = 'political_party'
PER_NAME = 'person_name'
PIC_URL = 'picture_url'
PERSON_URL = 'url'
ABC = list(ascii_lowercase)
TEMPLATE = 'http://www.parliament.lk/en/members-of-parliament/directory-of-members/?cletter={letter}'
TEMPLATE_2 = 'http://www.parliament.lk/members-of-parliament/directory-of-members/index2.php?option=com_members&task=all&tmpl=component&letter={letter}&wordfilter=&search_district='
PERSON_URL_TEMPLATE = 'http://www.parliament.lk/en/members-of-parliament/directory-of-members/viewMember/{person_id}'
URLS = [TEMPLATE_2.format(letter=_) for _ in ABC]


def custom_opener(url):
    import platform

    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False

    from helpers import fetch_string

    if _OS_LINUX:
        return fetch_string(url, cache_hours=6, use_curl=False)
    else:
        import requests
        try:
            get = requests.get(url).text
            return get
        except Exception, e:
            print e

def get_all_persons(url):
    page = custom_opener(url)
    elements = json.loads(page)

    for el in elements:
        person_id = el['mem_intranet_id']
        person_url = PERSON_URL_TEMPLATE.format(person_id=person_id)

        person_page = BeautifulSoup(custom_opener(person_url))
        image = person_page.find('div', {'class': 'left-pic'}).find('img').get('src')
        info = person_page.find('table', {'class': 'mem_profile'}).find_all('td')

        _person = {
            PER_NAME: el['member_sname_eng'],
            PERSON_URL: person_url,
            PIC_URL: image,
            POL_PARTY: info[0].find('a').text,
            POL_REG: str(info[1]).split('</div>')[-1].replace('</td>', '').strip()
        }

        yield _person


def get_entities(persons):
    def some_method(person):
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]

        return create_entity(unique_id, 'person', name, fields)

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
