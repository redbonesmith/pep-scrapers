# coding=utf-8
from utils import create_entity, create_id, custom_opener
import helpers

_host = 'http://www.assemblee-nationale.bj/'

URLS = [
    '{host}fr/deputes/listes-des-deputes'.format(host=_host)
]

POL_POS = 'political_position'
PER_NAME = 'person_name'
PIC_URL = 'picture_url'
PERSON_URL = 'url'

def _get_img_url(person_url):
    profile_page = custom_opener(person_url)
    image_link = profile_page.find('div', {'class': 'cb_field'}).find('img').get('src')
    return image_link


def _format_name(name_text, big_name):
    if big_name in name_text:
        name_with_space = big_name
        name_text = name_text.replace(big_name, '')
        return name_text + ' ' + name_with_space


def get_all_persons(url):
    page = custom_opener(url)

    all_rows = page. \
        find('table', {'class': 'cbUserListTable cbUserListT_4', 'id': 'cbUserTable'}). \
        find('tbody'). \
        find_all('tr')

    for row in all_rows:
        name, position = row.find_all('td')

        big_name = name.find('a').text
        name_text = name.text

        per_name = _format_name(name_text=name_text, big_name=big_name)
        per_url = name.find('a').get('href')
        pol_pos = position.text
        image = _get_img_url(per_url)

        obj = {
            PER_NAME: per_name,
            PERSON_URL: per_url,
            POL_POS: pol_pos,
            PIC_URL: image
        }
        yield obj

def get_entities(persons):
    entities = []
    for person in persons:
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]
        entities.append(create_entity(unique_id, 'person', name, fields))
    return entities


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            # helpers.check(entity)
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
