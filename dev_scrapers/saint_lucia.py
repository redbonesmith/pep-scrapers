# coding=utf-8
# /usr/bin/python -tt
from utils import create_entity, create_id, custom_opener
import helpers
import logging as log


BASE_PAGE = 'http://www.govt.lc'

URLS = [
    # '{page}/senate'.format(page=BASE_PAGE),
    '{page}/house-of-assembly'.format(page=BASE_PAGE),
]
"""
Contains a field "tag": "political_position", where value is given political position	Parliamentary Representative for Castries South-East
Contains a field "tag": "political_position", where value is given political position	Parliamentary Representative for Dennery South
"""

PER_NAME = 'person_name'
POLITICAL_PARTY = 'political_party'
POLITICAL_POSITION = 'political_position'
PIC_URL = 'picture_url'

WANTED_LIST = []

def bs_to_text_trim(_string):
    return _string.text.strip().lstrip().split(',')[0]

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('div', {'id': 'home-mid'}).find('tbody').find_all('tr')[1:]
    accumulator = []
    def get_data(row):
        person = {}
        td_list = row.find_all('td')
        
        for td in td_list:
            # print td
            if accumulator:
                person[POLITICAL_POSITION + '@accumulator'] = accumulator[-1]
            
            probably_img = td.find('img')
            
            if 'Sen.' in td.text or 'Hon.' in td.text:
                try:
                    name = name_cleaner(bs_to_text_trim(td.find('strong').extract()))
                    person[PER_NAME] = name
                    
                    #   polit info extraction
                    str_td = str(td)
                    
                    list_from_br = str_td.split('<br/>')[1:]
                    
                    cleaned = [x.replace('</span></td>', '').lstrip() for x in list_from_br]
                    
                    cleaned_2 = [x.split('>')[1].replace('</span></td>', '').replace('</a', '').lstrip() for x in cleaned if '>' in x]
                    cleaned_2 = [x for x in cleaned_2 if '<a data' not in x]
                    
                    profs = []
                    
                    for i in cleaned_2:
                        for x in cleaned:
                            if i in x:
                                cleaned.remove(x)
                                cleaned.append(i)
                    
                    for x in cleaned:
                        
                        text = x.strip('  ')
                        if text != '':
                            profs.append(text.strip().lstrip())
                        
                    if profs:
                        for enum, prof in enumerate(profs):
                            person[POLITICAL_POSITION + '@' + str(enum)] = prof
                    #   polit info extraction
                
                except AttributeError:
                    log.warning({'missing data in': row})
                    return None
                    
            elif probably_img:
                    person[PIC_URL] = BASE_PAGE + probably_img.get('src')
            else:
                probably_position = td.text
                if len(probably_position) > 1:
                    accumulator.append(probably_position)
                    

        return person        
                    
    persons = map(get_data, rows)
    persons = filter(None, persons)
    return persons            
            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t.split('@')[0], 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            # if write_to_mongo:
            #     yield entity
            # else:
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
