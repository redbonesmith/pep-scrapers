# /usr/bin/python -tt
# coding=utf-8
from utils import create_entity, create_id, custom_opener
import helpers


BASE_PAGE = 'http://www.parliament.gov.sg'

URLS = [
    '{page}/list-of-current-mps'.format(page=BASE_PAGE)
]

POL_REG = 'political_region'
POL_POS = 'political_position'
PER_NAME = 'person_name'
PIC_URL = 'picture_url'
PERSON_URL = 'url'


def name_cleaner(bad_name):
    try:
        return ' '.join(bad_name.text.split()[1:])
    except AttributeError:
        return ' '.join(bad_name.split()[1:])

def get_all_persons(url):
    page = custom_opener(url)
    main_table = page.find('div', {'class': 'view-content'}).find('tbody')
    rows = main_table.find_all('tr')

    for row in rows:
        name, region = row.find_all('td')[1:]
        person_url = BASE_PAGE+name.find('a').get('href')
        person_name = name.text

        person_page = custom_opener(person_url)
        person_info = person_page.find('div', {'id': 'particulars'})
        image = person_info.find('td', {'width': '200'}).find('img').get('src')
        position = str(person_info.find('td', {'width': '350'})).split('<br/>')[-1].strip().strip('</td>').strip('()')

        person = {
            PER_NAME: name_cleaner(person_name.strip()),
            POL_REG: region.text.strip(),
            PERSON_URL: person_url,
            PIC_URL: image.replace(' ', '%20'),
            POL_POS: position
        }

        yield person


def get_entities(persons):
    def some_method(person):
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]

        return create_entity(unique_id, 'person', name, fields)

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
