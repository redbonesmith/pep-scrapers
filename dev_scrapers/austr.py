# coding=utf-8
# /usr/bin/python -tt

import platform
import helpers
from hashlib import sha224
from re import sub
from datetime import datetime as dt
from bs4 import BeautifulSoup
from helpers import fetch_string
from urllib import quote
from dateutil import parser
from selenium import webdriver

from utils import custom_opener, create_entity, create_id
import helpers


#########################################
#   scraper body begins
#########################################

BASE_PAGE = 'http://www.parliament.vic.gov.au'

URLS = [
    '{page}/members/results?&page=1'.format(page=BASE_PAGE),
]


PER_NAME = 'person_name'
PIC_URL = 'picture_url'
DATE = 'date_of_birth'
PARTY = 'political_party'
POS = 'political_position'
URL = 'url'

WANTED_LIST = [PER_NAME, PIC_URL, DATE, PARTY, POS, URL]


def name_cleaner(bad_name):
    def strip(text):
        text = text.lstrip()
        return text.strip()
    good_name = ' '.join(map(strip, bad_name.split('\n'))).lstrip().strip().replace('Hon ', '').replace('Mr ', '')
    return good_name

def get_last_page(page):
    return int(page.find('div', {'class': 'pagination'}).find_all('a')[-2].text)

def generate_links(last_page):
    template = '{page}/members/results?&page={page_num}'
    return URLS + [template.format(page=BASE_PAGE, page_num=page_num) for page_num in range(last_page+1)[2:]]
    
def d1_parser(d1_list):
    """parsing d1 array"""
    last_key = []
    def create(x):
        key = x.find('dt').text.strip()
        value = x.find('dd').text.strip()
        if value:        
            if key:
                # clearing container before commiting
                del last_key[:]
                last_key.append(key)
            else:
                key = last_key[-1]
            return {key: value}
    return filter(None, map(create, d1_list))

def date_parser(data):
    if data:
        for el in data:
            if el.get('Personal'):
                comma_cutetted = el['Personal'].split(',')
                if 'Born' in comma_cutetted[0]: 
                    
                    parsed = parser.parse(comma_cutetted[0].replace('Born ', ''))
                    
                    return str(parsed).split(' ')[0]
    else:
        print 'no data in date_parser'

def party_parser(data):
    if data:
        for el in data:
            if el.get('Party'):
                party = dict(el)
                
                
                return party['Party']
    else:
        print 'no data in party_parser'

def pos_parser(data):
    if data:
        for el in data:
            if el.get('Positions'):
                pos = dict(el)
                data.rev
                return pos['Positions']
                
    else:
        print 'no data in pos_parser'


def get_all_persons(url):
    page = custom_opener(url)
    last_page = get_last_page(page)
    
    
    persons = []
    
    links_list = generate_links(last_page)
    
    for page_link in links_list:
        
        s = custom_opener(page_link)
        result_list = s.find('div', {'class': 'results-list'}).find_all('div', {'class': 'result-item'})
        def get_data(row):
            person = {}
            try:
                person[URL] = url = BASE_PAGE + row.find('div', {'class': 'item-title'}).find('a')['href']
                person[PER_NAME] = name =name_cleaner(row.find('div', {'class': 'item-title'}).find('a')['title'])
                
                person_page = custom_opener(url)
                person[PIC_URL] = person_page.find('img', {'class': 'details-portrait'})['src']
                _dl = person_page.find('div', {'class': 'list'}).find_all('dl')
                data = d1_parser(_dl)
                
                person[DATE] = date_parser(data)
                person[PARTY] = party_parser(data)
                
                if pos_parser(data):
                    person[POS] = pos_parser(data)
                
                import random
                hash = random.getrandbits(128)
                def code(symbol):
                    return symbol + "%032x" % hash
                
                for el in data:
                    for k, v in el.items():
                        if person.get(k):
                            person[k+ code('$')] = v
                            print person
                        else:
                            person[k] = v
                
            except Exception, e:
                pass
            return person
            
        persons += map(get_data, result_list)
    
    persons = filter(None, persons)
    print len(persons)
    return persons            


            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if '$' in t or t in WA:
                        fields.append({'tag': t.split('$')[0], 'value': v})
                    else:
                        fields.append({'name': t.split('@')[0], 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.check(entity)

# main scraper
if __name__ == "__main__":
    main()
