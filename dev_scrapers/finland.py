# coding=utf-8
# /usr/bin/python -tt
from utils import create_entity, create_id, custom_opener
import helpers
import logging as log
import re

BASE_PAGE = 'https://www.eduskunta.fi'

URLS = [
    '{page}/FI/kansanedustajat/nykyiset_kansanedustajat/Sivut/default.aspx'.format(page=BASE_PAGE),
]

"""
Rule Name	Rule Value
Contains given name or AKA entry, special field "name: person_name" field	Katri Kulmuni
Contains a field with "tag": "url", where the value is the url of the page containing person data	https://www.eduskunta.fi/FI/kansanedustajat/Sivut/565.aspx
Scrape minimum number of entities	40
Contains a field "tag": "picture_ul", with the value being the url of a picture on page	https://www.eduskunta.fi/FI/kansanedustajat/PublishingImages/Vehvilainen%20Anu_web_6707.jpg
Contains a field "tag": "political_region", where the value is given Region or Seat of Political Juristiction	Kaakkois-Suomen vaalipiiri
The test is specified custom_field_name:field_value. Contains a field "name": "xxxx", where xxxx is "custom_field_name", and the value of said field is "field_value"	Toimielinjäsenyydet ja tehtävät: Ulkoasiainvaliokunta (jäsen) 05.05.2015 - Puolustusvaliokunta (varajäsen) 05.05.2015 - Sosiaali- ja terveysvaliokunta (jäsen) 05.05.2015 - Suomen Pankin tilintarkastajat (jäsen) 12.09.2014 - Kansaneläkelaitoksen valtuutetut (jäsen) 30.06.2011 -
"""

PER_NAME = 'person_name'
POLITICAL_PARTY = 'political_party'
POLITICAL_REGION = 'political_region'
PERSON_URL = 'url'
PIC_URL = 'picture_url'

WANTED_LIST = []

def bs_to_text_trim(_string):
    return _string.text.strip().lstrip().split(',')[0]

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name


def get_all_persons(url):
    page = custom_opener(url, bs=True)
    x = page.find_all('div', {'allowdelete': 'false'})[1:-1]
    print '--'
    print x
    # l = [x.split('\r') for x in page.split('\n') if x != '\r']
    # print ''.join([x[0] for x in l])
    import sys
    exit(1)
    rows = page.find('tbody').find_all('tr')

    def get_data(row):
        td_list = row.find_all('td')
        _, image, name, position = td_list

        person = {}
        
        try:
            person[PER_NAME] = name_cleaner(bs_to_text_trim(name.find('a')))
            person[PERSON_URL] = BASE_PAGE + name.find('a')['href']
            person[PIC_URL] = BASE_PAGE + image.find('img')['src']
            
            #   sub-page processing            
            sub_page = custom_opener(person[PERSON_URL])
            try:
                collector = sub_page.find('div', {'class': 'keterangan mb40'}).find_all('div')
                
                person[POLITICAL_PARTY] = collector[-4].text.strip(':').lstrip()
                person[POLITICAL_REGION] = collector[-1].text.strip(':').lstrip()
            except Exception, e:
                log.warning({'msg': 'cant find political_region', 'obj': row})
            #   sub-page processing
            
            return person
            
            
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons            
            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            # if write_to_mongo:
            #     yield entity
            # else:
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
