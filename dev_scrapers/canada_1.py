# coding=utf-8
# /usr/bin/python -tt
from utils import create_entity, create_id, custom_opener, get_img
from bs4 import BeautifulSoup
import helpers
import logging as log

from urllib import quote
import dateutil.parser
BASE_PAGE = 'http://www.calgarycrimestoppers.org'

URLS = [
    '{page}/wanted'.format(page=BASE_PAGE),
]

PER_NAME = 'person_name'
PIC_URL = 'picture_url'
CUSTOM = 'custom_field_name'
TO_REMOVE = 'picker/manager/../../'
COMMA = '%2C'

WANTED_LIST = [CUSTOM]

def bs_to_text_trim(_string, multi=False):
    return _string.text.strip().lstrip() if not multi else ' '.join([bs_to_text_trim(x) for x in _string])

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find_all('div', {'class': 'wantedProfile'})

    def get_data(row):
        td_list = row.find_all('td')
        image,  _, _, name, k_age, age, k_descr, description, k_wanted, wanted, k_warrant, warrant = td_list
        
        date_sample = warrant.text.replace('Sept.', 'September')
        parsed_date = dateutil.parser.parse(date_sample)
        warrant = BeautifulSoup(str(parsed_date).split()[0]).find('p')
    
        keys = [[k_age, age], [k_descr, description], [k_wanted, wanted], [k_warrant, warrant]]
        
        person = {}
        
        try:
            person[PIC_URL] = quote(get_img(image)).replace(TO_REMOVE, '').replace(COMMA, ',')
            person[PER_NAME] = name_cleaner(bs_to_text_trim(name))
            
            for e, k in enumerate(keys):
                person[CUSTOM+'@{e}'.format(e=e)] = bs_to_text_trim(k, multi=True)
            
            return person
            
            
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons            
            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t.split('@')[0], 'value': v})
                    else:
                        fields.append({'name': t.split('@')[0], 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            if write_to_mongo:
                yield entity
            else:
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
