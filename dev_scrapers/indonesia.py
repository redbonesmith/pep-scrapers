# coding=utf-8
# /usr/bin/python -tt
from utils import create_entity, create_id, custom_opener
import helpers
import logging as log


BASE_PAGE = 'http://www.dpr.go.id'

URLS = [
    '{page}/en/anggota'.format(page=BASE_PAGE),
]


PER_NAME = 'person_name'
POLITICAL_PARTY = 'political_party'
POLITICAL_REGION = 'political_region'
PERSON_URL = 'url'
PIC_URL = 'picture_url'

WANTED_LIST = []

def bs_to_text_trim(_string):
    return _string.text.strip().lstrip().split(',')[0]

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('tbody').find_all('tr')

    def get_data(row):
        td_list = row.find_all('td')
        _, image, name, position = td_list

        person = {}
        
        try:
            person[PER_NAME] = name_cleaner(bs_to_text_trim(name.find('a')))
            person[PERSON_URL] = BASE_PAGE + name.find('a')['href']
            person[PIC_URL] = BASE_PAGE + image.find('img')['src']
            
            #   sub-page processing            
            sub_page = custom_opener(person[PERSON_URL])
            try:
                collector = sub_page.find('div', {'class': 'keterangan mb40'}).find_all('div')
                
                person[POLITICAL_PARTY] = collector[-4].text.strip(':').lstrip()
                person[POLITICAL_REGION] = collector[-1].text.strip(':').lstrip()
            except Exception, e:
                log.warning({'msg': 'cant find political_region', 'obj': row})
            #   sub-page processing
            
            return person
            
            
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons            
            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            if write_to_mongo:
                yield entity
            else:
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
