# coding=utf-8
# /usr/bin/python -tt
from utils import create_entity, create_id, custom_opener
import helpers
import logging as _log

BASE_PAGE = 'http://www.durango.gob.mx'

URLS = [
    '{page}/es/info_gobierno/gabinete'.format(page=BASE_PAGE),
]


POL_POS = 'political_position'
PER_NAME = 'person_name'
PERSON_PICTURE_URL = 'picture_url'
PERSON_URL = 'url'
WANTED_LIST = []


def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name
    
def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('section', {'id': 'mp-gabinete'}).find_all('li')
    
    def get_data(row):
        try:
            titular = {'class': 'gabinete-titular'}
            
            person_name = row.find('div', titular).find('a').text.strip().lstrip()
            person_url = row.find('div', titular).find('a').get('href')
            person_pic = row.find('img').get('src').replace('w:100', 'w:130').replace('mh:120', 'mh:170')
            person_position = row.find('div', {'class': 'gabinete-dep'}).find('a').text.strip().lstrip()
            
            return {
                    PER_NAME: name_cleaner(person_name),
                    PERSON_PICTURE_URL: BASE_PAGE + person_pic,
                    PERSON_URL: BASE_PAGE + person_url,
                    POL_POS: person_position
                }
        except AttributeError:
            _log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons
        
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
