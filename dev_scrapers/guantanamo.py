# coding=utf-8
# /usr/bin/python -tt
from utils import create_entity, create_id, custom_opener
import helpers
import logging as log


BASE_PAGE = 'https://wikileaks.org'

URLS = [
    '{page}/gitmo/name.html'.format(page=BASE_PAGE),
]


PER_NAME = 'person_name'
PERSON_URL = 'url'
DATE = 'date_of_birth'
PDF = 'PDF URL'
ISN = 'ISN'
COUNTRY = 'Country'
PLACE = 'place_of_birth'

WANTED_LIST = [ISN, COUNTRY, PDF]


def bs_to_text_trim(_string):
    return _string.text.strip().lstrip()

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name
    
def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find_all('a')
    rows = [x for x in rows if x.get('alt')]
    
    def get_data(row):
        person = {}
        
        try:
            person[PER_NAME] = name_cleaner(bs_to_text_trim(row))
            person[PERSON_URL] = BASE_PAGE + row['href']
            person[ISN] = row['alt']
            
            
            #   sub-page processing            
            sub_page = custom_opener(person[PERSON_URL])
            
            ##  pdf extract
            p = sub_page.find_all('p')
            person[PDF] = [BASE_PAGE + x.find('a').get('href') for x in p if 'PDF' in bs_to_text_trim(x)][0]
            ##  pdf extract

            sub_rows = sub_page.find_all('tr')
            
            c = {}
            for i in sub_rows:
                k = i.find('th')
                v = i.find('td')
                k, v = map(bs_to_text_trim, [k, v])
                c[k] = v 
            
            try:
                msg = 'cant find || {subject} ||'
                
                date_ = helpers.get_date_text(c['Birth date'])
                
                country = c[COUNTRY] if c[COUNTRY] else log.warning({'msg': msg.format(subject=COUNTRY), 'obj': c})
                place = c['Place of birth'] if c['Place of birth'] else log.warning({'msg': msg.format(subject=PLACE), 'obj': c})
                date_ = date_ if date_ else log.warning({'msg': msg.format(subject=DATE), 'obj': c})
                
                person[DATE] = date_
                person[COUNTRY] = country
                person[PLACE] = place
                
            except Exception:
                log.warning({'msg': 'cant parse object', 'obj': c})
                
            #   sub-page processing
            
            
            return person
            
            
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons
        
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            if write_to_mongo:
                yield entity
            else:
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
