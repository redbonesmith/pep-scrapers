# coding=utf-8
# /usr/bin/python -tt
# coding=utf-8
from utils import create_entity, create_id, custom_opener
import helpers

BASE_PAGE = 'http://www.parlament.mt'

URLS = [
    '{page}/membersofparliament?l=1'.format(page=BASE_PAGE)
]

POL_POS = 'political_position'
PER_NAME = 'person_name'
PIC_URL = 'picture_url'
PERSON_URL = 'url'


def _opener(url):
    return custom_opener(url)


def _get_column2(soup):
    return soup.find('div', {'class': 'column2'})


def _get_position(soup, get_column2):
    pol_pos_in_name = get_column2(soup).find('h1').text.split('-')
    if len(pol_pos_in_name) is 2:
        return pol_pos_in_name[-1].lstrip()


def _get_img_url(soup):
    image_link = soup.find('td', {'dir': 'ltr'}).find('img').get('src')
    return image_link

def _format_name(name_text, big_name):
    if big_name in name_text:
        name_with_space = big_name
        name_text = name_text.replace(big_name, '')
        return name_text + ' ' + name_with_space


def _change_person_name(default_name):
    name_split = default_name.split()
    name_split.insert(0, name_split.pop())
    return ' '.join(name_split)


def _create_url(suffix):
    string = '{base}/{suffix}'
    return string.format(base=BASE_PAGE, suffix=suffix)

def get_all_persons(url):
    page = custom_opener(url)
    table_div = _get_column2(page)
    rows = table_div.find('table', {'summary': 'Design Layout'}).find_all('tr')
    couples = [row.find_all('td') for row in rows]

    for couple in couples:
        if couple:
            for person in couple:
                try:
                    link = person.find('a')
                    person_url = _create_url(link.get('href'))
                    sub_page_soup = _opener(person_url)
                    person_name = _change_person_name(link.text)
                    image_url = _create_url(_get_img_url(sub_page_soup))
                    position = _get_position(sub_page_soup, _get_column2)
                    obj = {
                        PERSON_URL: person_url,
                        PER_NAME: person_name,
                        PIC_URL: image_url
                    }

                    if position:
                        obj[POL_POS] = position

                    yield obj

                except:
                    continue


def get_entities(persons):
    def some_method(person):
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]

        return create_entity(unique_id, 'person', name, fields)

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.check(entity)
            # helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()