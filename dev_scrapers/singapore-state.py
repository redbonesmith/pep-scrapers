# /usr/bin/python -tt
# coding=utf-8
from utils import create_entity, create_id, custom_opener
import helpers


BASE_PAGE = 'http://app.sgdi.gov.sg'

URLS = [
    '{page}/listing.asp?agency_subtype=dept&agency_id=0000004737'.format(page=BASE_PAGE)
]

POL_POS = 'political_position'
PER_NAME = 'person_name'
PERSON_URL = 'url'


def name_cleaner(bad_name):
    words = ['Ms ', 'Mr ', 'Mrs ']
    good_name = bad_name.strip()
    
    for word in words:
        if word in good_name:
            return good_name.replace(word, '')
    
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('table', {'class': 'peopleList'}).find_all('tr')[2:]
    
    for row in rows:
        position, name = row.find_all('td')[:2]
        
    
        yield {POL_POS: position.text, PER_NAME: name_cleaner(name.text)}


def get_entities(persons):
    def some_method(person):
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]

        return create_entity(unique_id, 'person', name, fields)

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
