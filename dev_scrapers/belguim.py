# coding=utf-8
# /usr/bin/python -tt
from utils import create_entity, create_id, custom_opener
import helpers
import logging as log


BASE_PAGE = 'http://www.dekamer.be'

URLS = [
    '{page}/kvvcr/showpage.cfm?section=/depute&language=nl&cfm=/site/wwwcfm/depute/cvlist.cfm'.format(page=BASE_PAGE),
]


PER_NAME = 'person_name'
POLITICAL_PARTY = 'political_party'
PERSON_URL = 'url'
PIC_URL = 'picture_url'

WANTED_LIST = []

def bs_to_text_trim(_string):
    # return _string.text.strip().lstrip()
    return ' '.join([x.strip().lstrip() for x in _string.text.split(' ')])

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name
    
def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find_all('div', {'id': 'story'})[2].find_all('tr')
    def get_data(row):
        td_list = row.find_all('td')
        name, political_party, _, _ = td_list

        person = {}
        
        try:
            person[PER_NAME] = name_cleaner(bs_to_text_trim(name))
            person[PERSON_URL] = BASE_PAGE + '/kvvcr/' + name.find('a')['href']
            person[POLITICAL_PARTY] = bs_to_text_trim(political_party)
            
            #   sub-page processing            
            sub_page = custom_opener(person[PERSON_URL])
            try:
                person[PIC_URL] = BASE_PAGE + sub_page.find('img', {'alt': 'Picture'})['src']
            except Exception, e:
                log.warning({'msg': 'cant find picture_url', 'obj': row})
            #   sub-page processing
            
            return person
            
            
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons
        
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
