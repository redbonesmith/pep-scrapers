# coding=utf-8
# /usr/bin/python -tt

import platform
import helpers
from hashlib import sha224
from re import sub
from datetime import datetime as dt
from bs4 import BeautifulSoup
from helpers import fetch_string
from urllib import quote
from dateutil import parser


def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default

def create_id(args):
    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()

def custom_opener(url):
    return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
#########################################
#   scraper body begins
#########################################


BASE_PAGE = 'http://tamaulipas.gob.mx'

URLS = [
    '{page}/gobierno/directorio-telefonico/'.format(page=BASE_PAGE),
]


PER_NAME = 'person_name'
POS = 'political_position'


def name_cleaner(bad_name):
    black_list = ['Lic. ', 'Ing. ']

    def strip(text):
        text = text.lstrip()
        return text.strip()

    def replacer(text):
        _replace = []
        for prefix in black_list:
            if prefix in text:
                _replace.append(text.replace(prefix, ''))
        
        return _replace[-1] if _replace else text
            
    good_name = ' '.join(map(strip, bad_name.split('\n'))).lstrip().strip()

    return replacer(good_name)


def get_all_persons(url):
    page = custom_opener(url)
    trows = page.find('tbody').find_all('tr')[1:]
    
    persons = []
    
    for row in trows:
        def get_data(row):
            person = {}
            try:
                name, position, email, _ =row.find_all('td')
                person[PER_NAME] = name_cleaner(name.text)
                person[POS] = position.text
                try:
                    email = email.text
                    person['email'] = email
                except Exception:
                    pass
                
            except Exception:
                pass
            
            return person
            
        persons += map(get_data, trows)
    
    persons = filter(None, persons)
    
    return persons            

def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    fields.append({'tag': t, 'value': v})
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]

def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
