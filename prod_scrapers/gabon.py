# coding=utf-8
# /usr/bin/python -tt
# coding=utf-8
# coding=utf-8
# /usr/bin/python -tt

def create_entity(_id, entity_type, obj_name, fields, aka=False):

	default = {
		"_meta": {
		"id": _id,
		"entity_type": entity_type
		},
		"name": obj_name,
		"fields": fields
	}

	if aka:
		default.update({'aka': aka})

	return default


def create_id(args):

	from hashlib import sha224
	from re import sub

	if not isinstance(args, list):
		args = [args]
	conc_names = ''.join([_.decode('utf-8') for _ in args])
	return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
	import platform

	_OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False

	from bs4 import BeautifulSoup
	from helpers import fetch_string

	if _OS_LINUX:
		try:
			return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False, num_tries=6))
		except Exception, e:
			print 'Exception in fetch_string, url is: ', url
		else:
			from urllib2 import urlopen

		try:
			return BeautifulSoup(urlopen(url, timeout=20).read())
		except Exception, e:
			print e
import helpers
from string import ascii_lowercase, digits, maketrans
from dateutil import parser

BASE_PAGE = 'http://www.senat.ga.demo.nic.ga'

URLS = [
    '{page}/ordre-senateurs.twg'.format(page=BASE_PAGE)
]

POL_POS = 'political_position'
PER_NAME = 'person_name'
PIC_URL = 'picture_url'
DATE_OF_BIRTH = 'date_of_birth'
PERSON_URL = 'url'
ABC = list(ascii_lowercase)
PATTERN = 'http://www.senat.ga.demo.nic.ga/ordre-senateurs.twg?ordre={letter}'
URLS = [PATTERN.format(letter=_) for _ in ABC]

def _create_url(suffix):
	string = '{base}/{suffix}'
	return string.format(base=BASE_PAGE, suffix=suffix)

def get_person_urls_from_page(soup):
	try:
		alphabet = soup.find('ul', {'class': 'alphabet'})
		if alphabet:
			get_all_li = alphabet.find('ul').find_all('li')
			return [BASE_PAGE + _.find('a').get('href') for _ in get_all_li]
	except Exception, e:
		print 'Exception in get_names_from_page', e

def get_person_name(soup):
	return soup.find('div', {'class': 'title_rub'}).text.strip()

def get_img(soup):
	debug = None
	try:
		for_debug = soup.find('div', {'class': 'statut_president'})
		debug = for_debug
		return BASE_PAGE + for_debug.find('img').get('src')
	except Exception, err:
		print 'Error in getting picture', err

def get_b_day(soup):
	date_for_debug = None
	try:
		x = soup.find('ul', {'class': 'bio'}).find_all('li')[0].find('ul').find('li').text
		bday_block = x.encode('utf-8')
		
		
		date = bday_block.split()[-3:]
		
		if len(date) is 3:
			
			all_ = maketrans('','')
			nodigs = all_.translate(all_, digits)
			date[0] = date[0].encode('utf-8').translate(all_, nodigs)

			
			
			if 'Jui' in date[1]:
				date[1] = 'July'
			if 'Jiun' in date[1]:
				date[1] = 'June'
				
			if 'Mai' in date[1]:
				date[1] = 'May'
			
			if date[1].startswith('Avr'):
				date[1] = 'April'
			if date[1].startswith('Mar'):
				date[1] = 'March'
			
			date[1] = date[1][0:3]
			

			return str(parser.parse(' '.join(date))).split()[0]
	except Exception, err:
		print 'error in date', err
		
		
def get_all_persons(url):
	page = custom_opener(url)
	
	for person_url in get_person_urls_from_page(page):
		person_soup = custom_opener(person_url)
		person_pic = get_img(person_soup)
		b_date = get_b_day(person_soup)
		person = {
			PER_NAME: get_person_name(person_soup),
			PERSON_URL: person_url,
		}
		
		if person_pic:
			person[PIC_URL] = person_pic
			
		if b_date:
			person[DATE_OF_BIRTH] = b_date
		
		yield person

def get_entities(persons):
	def some_method(person):
		name = person.pop(PER_NAME)
		values = person.values()
		unique_id = create_id([_.encode('utf-8') for _ in values])
		fields = [{'tag': t, 'value': v} for t, v in person.items()]

		return create_entity(unique_id, 'person', name, fields)

	return [element for element in map(some_method, persons) if element if not None]


def main():
	for url in URLS:
		main_obj = get_all_persons(url)

		for entity in get_entities(main_obj):
			
			helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()