# coding=utf-8
# /usr/bin/python -tt

######################
# NOTE
# YOU NEED TO ADD THIS LINE TO HELPERS 
# requests.packages.urllib3.disable_warnings()
######################

import logging as log

log.basicConfig(filename='scraper.log',level=log.DEBUG)
log.info('Starting LOGGING scraper')


import platform
import helpers
from hashlib import sha224
from re import sub
from datetime import datetime as dt
from bs4 import BeautifulSoup
from helpers import fetch_string


def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False
    
    if _OS_LINUX:
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    else:
        import requests
        try:
            get = requests.get(url).text
            soup = BeautifulSoup(get)
            return soup
        except Exception, e:
            log.exception({'error': e})
            
#########################################
#   scraper body begins
#########################################


BASE_PAGE = 'https://wikileaks.org'

URLS = [
    '{page}/gitmo/name.html'.format(page=BASE_PAGE),
]


PER_NAME = 'person_name'
PERSON_URL = 'url'
DATE = 'date_of_birth'
PDF = 'PDF URL'
ISN = 'ISN'
COUNTRY = 'Country'
PLACE = 'place_of_birth'

WANTED_LIST = [ISN, COUNTRY, PDF]


def bs_to_text_trim(_string):
    return _string.text.strip().lstrip()

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name
    
def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find_all('a')
    rows = [x for x in rows if x.get('alt')]
    
    def get_data(row):
        person = {}
        
        try:
            person[PER_NAME] = name_cleaner(bs_to_text_trim(row))
            person[PERSON_URL] = BASE_PAGE + row['href']
            person[ISN] = row['alt']
            
            
            #   sub-page processing            
            sub_page = custom_opener(person[PERSON_URL])
            
            ##  pdf extract
            p = sub_page.find_all('p')
            person[PDF] = [BASE_PAGE + x.find('a').get('href') for x in p if 'PDF' in bs_to_text_trim(x)][0]
            ##  pdf extract

            sub_rows = sub_page.find_all('tr')
            
            c = {}
            for i in sub_rows:
                k = i.find('th')
                v = i.find('td')
                k, v = map(bs_to_text_trim, [k, v])
                c[k] = v 
            
            try:
                msg = 'cant find || {subject} ||'
                
                date_ = helpers.get_date_text(c['Birth date'])
                
                country = c[COUNTRY] if c[COUNTRY] else log.debug({'msg': msg.format(subject=COUNTRY), 'obj': c})
                place = c['Place of birth'] if c['Place of birth'] else log.debug({'msg': msg.format(subject=PLACE), 'obj': c})
                date_ = date_ if date_ else log.debug({'msg': msg.format(subject=DATE), 'obj': c})
                
                person[DATE] = date_
                person[COUNTRY] = country
                person[PLACE] = place
                
            except Exception:
                log.debug({'msg': 'cant parse object', 'obj': c})
            #   sub-page processing
            
            
            return person
            
            
        except AttributeError:
            log.debug({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons
        
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
    
