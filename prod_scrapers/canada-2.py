# coding=utf-8
# /usr/bin/python -tt

import platform
import helpers
from hashlib import sha224
from re import sub
from datetime import datetime as dt
from bs4 import BeautifulSoup
from helpers import fetch_string
from urllib import quote
from dateutil import parser
from selenium import webdriver

def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()

def custom_opener(url):
    return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))


def get_img(bs, attr=False):
    try:
        return bs.find('img', attr=attr)['src']
        
    except Exception, e:
        print({'error': e, 'in': bs})

#########################################
#   scraper body begins
#########################################

BASE_PAGE = 'http://www.edmontonpolice.ca'

URLS = [
    '{page}/CrimeFiles/EdmontonsMostWanted.aspx'.format(page=BASE_PAGE),
]

PER_NAME = 'person_name'
PIC_URL = 'picture_url'
DATE = 'date_of_birth'
CUSTOM = 'custom_field_name'
BIO = 'BIO'

WANTED_LIST = ['height', 'weight']


def name_cleaner(bad_name):
    def strip(text):
        text = text.lstrip()
        return text.strip()
    good_name = ' '.join(map(strip, bad_name.split('\n'))).lstrip()
    return good_name

def get_last_page(page):
    return int(page.find('select').find_all('option').pop().text)

def click(driver, page_number):
    return driver.find_element_by_xpath("//select[@name='phmain_0$phcontent_0$grdPager$currentPage']/option[text()='{page}']".format(page=page_number)).click()

def get_all_persons(url):
    last_page = get_last_page(custom_opener(url))
    driver = webdriver.PhantomJS()
    driver.get(url)
    page_content = (click(driver, x) for x in range(1, last_page+1))
    persons = []
    for _ in page_content:
        s = BeautifulSoup(driver.page_source)
        rows = s.find('tbody').find_all('tr')

        def get_data(row):
            person = {}
            try:
                _class = row.find('div', {'class': 'mostWanted'})
                
                person_url = BASE_PAGE + _class.find('a').get('href')
                person['url'] = person_url
                person_page = custom_opener(person_url)
                block = person_page.find('div', {'id': 'mostWanted'})
                person[PER_NAME] = name_cleaner(block.find('h1').text).strip()
                person[PIC_URL] = BASE_PAGE + _class.find('img').get('src').replace('..', '').replace('?w=150', '')
                date = _class.find('div', {'class': 'age'}).text.strip().split(':')[-1].lower()
                
                if len(date) == 2 or 'old' in date:
                    person[DATE] = date
                else:
                    person[DATE] = str(parser.parse(date)).split(' ')[0]
                height = _class.find('div', {'class': 'height'}).text.strip().split(':')[-1]
                person['height'] = height.replace('\\', '') if 'cm' not in height else height.split(' ')[-1].strip('()').replace('\\', '')
                person['weight'] = _class.find('div', {'class': 'weight'}).text.strip().split(':')[-1]
                person['description'] = _class.find('div', {'class': 'description'}).text.strip()
                person['content'] = _class.find('div', {'class': 'content'}).text.strip()
                
                
            except Exception,_:
                pass
            return person
            
        persons += map(get_data, rows)
        
    
    persons = filter(None, persons)
    return persons            
            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t.split('@')[0], 'value': v})
                    else:
                        fields.append({'name': t.split('@')[0], 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
