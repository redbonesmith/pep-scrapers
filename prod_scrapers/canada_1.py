# coding=utf-8
# /usr/bin/python -tt

import logging as log
log.basicConfig(filename='scraper.log',level=log.DEBUG)
log.info('Starting LOGGING scraper')

import platform
import helpers
from hashlib import sha224
from re import sub
from datetime import datetime as dt
from bs4 import BeautifulSoup
from helpers import fetch_string
from urllib import quote
import dateutil.parser


def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()

def custom_opener(url):
    try:    
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    except Exception, e:
        log.exception({'error': e})


def get_img(bs, attr=False):
    try:
        return bs.find('img', attr=attr)['src']
        
    except Exception, e:
        log.exception({'error': e, 'in': bs})

#########################################
#   scraper body begins
#########################################

BASE_PAGE = 'http://www.calgarycrimestoppers.org'

URLS = [
    '{page}/wanted'.format(page=BASE_PAGE),
]

PER_NAME = 'person_name'
PIC_URL = 'picture_url'
CUSTOM = 'custom_field_name'
TO_REMOVE = 'picker/manager/../../'
COMMA = '%2C'
SEP = '%3A'
AGE = 'AGE'


WANTED_LIST = [CUSTOM]

def bs_to_text_trim(_string, multi=False):
    return _string.text.strip().lstrip() if not multi else ' '.join([bs_to_text_trim(x) for x in _string])

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find_all('div', {'class': 'wantedProfile'})

    def get_data(row):
        td_list = row.find_all('td')
        image,  _, _, name, k_age, age, k_descr, description, k_wanted, wanted, k_warrant, warrant = td_list
        
        date_sample = warrant.text.replace('Sept.', 'September')
        parsed_date = dateutil.parser.parse(date_sample)
        warrant = BeautifulSoup(str(parsed_date).split()[0]).find('p')
    
        keys = [[k_age, age], [k_descr, description], [k_wanted, wanted], [k_warrant, warrant]]
        
        person = {}
        
        try:
            person[PIC_URL] = quote(get_img(image)).replace(TO_REMOVE, '').replace(COMMA, ',').replace(SEP, ':')
            person[PER_NAME] = name_cleaner(bs_to_text_trim(name))
            person[AGE] = bs_to_text_trim(age)
            person[] = ''
            # for e, k in enumerate(keys):
            #     person[CUSTOM+'@{e}'.format(e=e)] = bs_to_text_trim(k, multi=True)
            
            return person
            
            
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons            
            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t.split('@')[0], 'value': v})
                    else:
                        fields.append({'name': t.split('@')[0], 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            # if write_to_mongo:
            #     yield entity
            # else:
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
