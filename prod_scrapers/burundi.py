# coding=utf-8
# /usr/bin/python -tt

import logging as log
log.basicConfig(filename='scraper.log',level=log.DEBUG)
log.info('Starting LOGGING scraper')

import platform
import helpers
from hashlib import sha224
from re import sub
from datetime import datetime as dt
from bs4 import BeautifulSoup
from helpers import fetch_string


def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
    try:    
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    except Exception, e:
        log.exception({'error': e})

#########################################
#   scraper body begins
#########################################



BASE_PAGE = 'http://www.assemblee.bi'

URLS = [
    '{page}/Liste-des-deputes'.format(page=BASE_PAGE),
]

PER_NAME = 'person_name'
POLITICAL_PARTY = 'political_party'
POLITICAL_REGION = 'political_region'

WANTED_LIST = []

def bs_to_text_trim(_string):
    return _string.text.strip().lstrip().split(',')[0]

def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('div', {'class': 'texte'}).find_all('tr')[1:]

    def get_data(row):
        td_list = row.find_all('td')
        _, name, region, party = td_list

        person = {}
        
        try:
            person[PER_NAME] = name_cleaner(bs_to_text_trim(name))
            person[POLITICAL_REGION] = name_cleaner(bs_to_text_trim(region))
            person[POLITICAL_PARTY] = name_cleaner(bs_to_text_trim(party))
            
            return person
            
            
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons            
            
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main(write_to_mongo=False):
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main(write_to_mongo=False)
