# /usr/bin/python -tt
# coding=utf-8
def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    from hashlib import sha224
    from re import sub

    if not isinstance(args, list):
        args = [args]
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
    import platform

    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False

    from bs4 import BeautifulSoup
    from helpers import fetch_string

    if _OS_LINUX:
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    else:
        import requests
        try:
            get = requests.get(url).text
            soup = BeautifulSoup(get)
            return soup
        except Exception, e:
            print e
import helpers


BASE_PAGE = 'http://app.sgdi.gov.sg'

URLS = [
    '{page}/listing.asp?agency_subtype=dept&agency_id=0000004737'.format(page=BASE_PAGE)
]

POL_POS = 'political_position'
PER_NAME = 'person_name'
PERSON_URL = 'url'


def name_cleaner(bad_name):
    words = ['Ms ', 'Mr ', 'Mrs ']
    good_name = bad_name.strip()
    
    for word in words:
        if word in good_name:
            return good_name.replace(word, '')
    
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('table', {'class': 'peopleList'}).find_all('tr')[2:]
    
    for row in rows:
        position, name = row.find_all('td')[:2]
        
    
        yield {POL_POS: position.text, PER_NAME: name_cleaner(name.text)}


def get_entities(persons):
    def some_method(person):
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]

        return create_entity(unique_id, 'person', name, fields)

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
