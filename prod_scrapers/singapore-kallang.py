# coding=utf-8
# /usr/bin/python -tt
def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    from hashlib import sha224
    from re import sub
    from datetime import datetime as dt

    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
    import platform

    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False

    from bs4 import BeautifulSoup
    from helpers import fetch_string

    if _OS_LINUX:
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    else:
        import requests
        try:
            get = requests.get(url).text
            soup = BeautifulSoup(get)
            return soup
        except Exception, e:
            print e

import helpers

BASE_PAGE = 'http://www.mktc.org.sg'

URLS = [
    '{page}/www/?town-councillors,35'.format(page=BASE_PAGE),
    '{page}/www/?the-committees,36'.format(page=BASE_PAGE)
]

POL_POS = 'political_position'
PER_NAME = 'person_name'
COMITE = 'Committee'
PERSON_URL = 'url'
SEPARATOR = '<br/'

WANTED_LIST = [COMITE]


def name_cleaner(bad_name):
    
    bad_name = bad_name.split(',')[0]
    
    words = ['Ms ', 'Mr ', 'Mrs ']
    good_name = bad_name.strip()
    
    for word in words:
        if word in good_name:
            return good_name.replace(word, '')
    
    return good_name

def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('table', {'width': '900', 'cellspacing': '2', 'cellpadding': '1'}).find_all('tr')
    c = []
    for row in rows:
        position_container, name_container = row.find_all('td')
        
        committee = position_container.text.strip() if COMITE in position_container.text else None
        c.append(committee)
        position = position_container.text.strip() if COMITE not in position_container.text else None
        people = name_container.find_all('p')
        c = filter(None, c)
        if people:
            for person in people:
                obj = {PER_NAME: name_cleaner(person.text), POL_POS: position}
                if c:
                    obj[COMITE] = c[-1]
                yield obj
                
        else:
            name_text = str(name_container)
            
            if SEPARATOR in name_text:
                
                names = name_text.split('>')
                names = [_.strip(SEPARATOR) for _ in names if SEPARATOR in _]
                names = [_ for _ in names if _]
                
                for name in names:
                    obj = {PER_NAME: name_cleaner(name), POL_POS: position}
                    if c:
                        obj[COMITE] = c[-1]
                    
                    yield obj
                    
            else:
                name = name_text.split('>')[1].split('</td')[0].strip()
                # name = name.replace(u'\u00a0', '')
                
                if name:
                    obj = {PER_NAME: name_cleaner(name), POL_POS: position}
                    if c:
                        obj[COMITE] = c[-1]
                    
                    yield obj
            
        

def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                # fields = [{'tag': t, 'value': v} for t, v in person.items()]
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
