# coding=utf-8
# /usr/bin/python -tt

######################
# NOTE
# YOU NEED TO ADD THIS LINE TO HELPERS 
# requests.packages.urllib3.disable_warnings()
######################

import logging as log

log.basicConfig(filename='scraper.log',level=log.DEBUG)
log.info('Starting LOGGING scraper')


import platform
import helpers
from hashlib import sha224
from re import sub
from datetime import datetime as dt
from bs4 import BeautifulSoup
from helpers import fetch_string


def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False
    
    if _OS_LINUX:
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    else:
        import requests
        try:
            get = requests.get(url).text
            soup = BeautifulSoup(get)
            return soup
        except Exception, e:
            log.exception({'error': e})
            
#########################################
#   scraper body begins
#########################################


BASE_PAGE = 'http://www.durango.gob.mx'

URLS = [
    '{page}/es/info_gobierno/gabinete'.format(page=BASE_PAGE),
]


POL_POS = 'political_position'
PER_NAME = 'person_name'
PERSON_PICTURE_URL = 'picture_url'
PERSON_URL = 'url'
WANTED_LIST = []


def name_cleaner(bad_name):
    _separator = '.'
    good_name = bad_name.split(_separator)[-1].lstrip()
    return good_name
    
def get_all_persons(url):
    page = custom_opener(url)
    rows = page.find('section', {'id': 'mp-gabinete'}).find_all('li')
    
    def get_data(row):
        try:
            titular = {'class': 'gabinete-titular'}
            
            person_name = row.find('div', titular).find('a').text.strip().lstrip()
            person_url = row.find('div', titular).find('a').get('href')
            person_pic = row.find('img').get('src').replace('w:100', 'w:130').replace('mh:120', 'mh:170')
            person_position = row.find('div', {'class': 'gabinete-dep'}).find('a').text.strip().lstrip()
            
            return {
                    PER_NAME: name_cleaner(person_name),
                    PERSON_PICTURE_URL: BASE_PAGE + person_pic,
                    PERSON_URL: BASE_PAGE + person_url,
                    POL_POS: person_position
                }
        except AttributeError:
            log.warning({'missing data in': row})
            return None
        
    persons = map(get_data, rows)
    persons = filter(None, persons)
    
    
    return persons
        
def get_entities(persons):
    def some_method(person):
        try:
            if person[PER_NAME] and u'\u00a0' not in person[PER_NAME]:
                name = person.pop(PER_NAME)
                values = person.values()
                unique_id = create_id([_.encode('utf-8') for _ in values])
                
                fields = []
                for t, v in person.items():
                    if t not in WANTED_LIST:
                        fields.append({'tag': t, 'value': v})
                    else:
                        fields.append({'name': t, 'value': v})
    
        
                return create_entity(unique_id, 'person', name, fields)
        except:
            pass

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
