# coding=utf-8
# /usr/bin/python -tt

def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    from hashlib import sha224
    from re import sub

    if not isinstance(args, list):
        args = [args]
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
    import platform

    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False

    from bs4 import BeautifulSoup
    from helpers import fetch_string

    if _OS_LINUX:
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    else:
        from urllib2 import urlopen

        try:
            return BeautifulSoup(urlopen(url, timeout=20).read())
        except Exception, e:
            print e
            pass


import helpers

BASE_PAGE = 'https://www.supremecourt.gov.sg'
INIT_PAGE = '{base}/default.aspx?pgID=40'.format(base=BASE_PAGE)

URLS = [
    INIT_PAGE
]

PER_NAME = 'person_name'
POLITICAL_POSITION = 'political_position'
BIO = 'biography'
PIC_URL = 'picture_url'
PERSON_URL = 'url'
TEXT = "The Supreme Court Registry is currently headed by the Registrar of the Supreme Court. He is assisted by the Deputy Registrar, Senior Assistant Registrars and the Assistant Registrars who perform judicial functions. Certain civil proceedings in the High Court, which are heard in chambers, are dealt with by the Registrars."


def extract_name_from_image_url(img_url):
    import re

    slugs = ['(2015).jpg', 'JC', 'Justice', 'CJ', '.jpg', '2015', 'SDJ']
    name_to_process = img_url.split('/40/')[-1]

    for slug in slugs:
        name_to_process = name_to_process.replace(slug, '')

    if '%20' in name_to_process:
        name_to_process = name_to_process.replace('%20', ' ')

    if ' ' not in name_to_process:
        name_to_process = ' '.join([a for a in re.split(r'([A-Z][a-z]*)', name_to_process) if a])

    return name_to_process.strip()


def get_all_person_images(soup):
    separator = '/40/'

    def url_fix(url):
        if 'http' not in url:
            return BASE_PAGE + url
        else:
            return url

    all_img = soup.find_all('img')
    persons_pictures = [_.get('src') for _ in all_img if separator in _.get('src') if
                        _.get('src').endswith('.jpg')]

    persons_pictures = ['/data/media/ManagePage' + separator + _.split(separator)[-1] for _ in persons_pictures]

    return map(url_fix, persons_pictures)


def get_all_bios(soup):
    uls = soup.find_all('ul')
    bios = [_.text.strip().replace(u'\r\n', '') for _ in uls if 'Appointed' in _.text]
    bios[0] += ' %s' % bios.pop(1)
    return bios


def get_all_regs(soup):
    def name_cleaner(bad_name):
        try:
            return ' '.join(bad_name.text.split()[1:])
        except AttributeError:
            return ' '.join(bad_name.split()[1:])

    def process_assist(obj):
        position = obj.find('p').text.strip()
        people = obj.find_all('li')

        for person in people:
            yield {
                PER_NAME: name_cleaner(person.text),
                POLITICAL_POSITION: position,
                # BIO: TEXT
            }

    def process_top_two(people):
        for i in people:
            x = [_.text.strip().strip() for _ in i.find_all('font')]
            x = [_ for _ in x if _]
            position, name = set(x)

            yield {
                PER_NAME: name_cleaner(name),
                POLITICAL_POSITION: position
            }

    def process_three_top(a, b):
        position = a.text.strip()
        two = b.find_all('font')

        first = [_.text for _ in two[2:]][0]
        last = b.find_all('li')[-1].text.strip()
        second = two[-1].text

        for name in [first, second, last]:
            yield {
                PER_NAME: name_cleaner(name),
                POLITICAL_POSITION: position
            }

    regs = soup.find_all('blockquote')[1:]
    assists = process_assist(regs[-1])
    all_paragraphs = regs[0].find_all('p')
    top_two = process_top_two(all_paragraphs[:2])
    top_three = process_three_top(all_paragraphs[-1], regs[0].find_all('li')[-3:][0])

    for generator in [assists, top_two, top_three]:
        for name in generator:
            name[BIO] = TEXT
            yield name


def get_all_positions(soup, names):
    first = soup.find_all('td', {'bgcolor': '#fdfcf7', 'width': '100%'})[0]
    all_instead_main = soup.find_all('p', {'align': 'center'})
    all_instead_main.insert(0, first)
    for position, name in zip(all_instead_main, names):
        yield position.text.split(name)[0].strip()


def get_all_persons(page):
    pictures = get_all_person_images(page)
    names = map(extract_name_from_image_url, pictures)
    bios = get_all_bios(page)
    positions = get_all_positions(page, names)

    for person_data in zip(pictures, names, bios, positions):
        picture, name, bio, position = person_data
        yield {
            PIC_URL: picture,
            PER_NAME: name,
            POLITICAL_POSITION: position,
            BIO: bio
        }


def get_entities(persons):
    def some_method(person):
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]

        return create_entity(unique_id, 'person', name, fields)

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        page = custom_opener(url)
        main_obj = get_all_persons(page)
        second_obj = get_all_regs(page)

        objects = [main_obj, second_obj]

        for obj in objects:
            for entity in get_entities(obj):
                helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
