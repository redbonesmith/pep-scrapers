# coding=utf-8
def create_entity(_id, entity_type, obj_name, fields, aka=False):
    """
    easy create entity using input data

    :param _id: unique id of entity (use create_id(args))
    :param entity_type: (should be string or unicode) organisation | person etc
    :param obj_name: formal name => 'Vasya Pupkin'
    :param fields: list with {tags|names and values} => {'tag': 'political_position', 'value': 'director'}
    :param aka: list with aka names => aka = [{'name': 'Some AKA Name'}, {'name': 'Some AKA Name'}]
    :return: dict
    """
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default

def create_id(args):
    """
    Generate ID for entity
    :param args: strings
    :return: hashsum
    """
    from hashlib import sha224
    from re import sub

    if not isinstance(args, list):
        args = [args]
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()

def custom_opener(url):
    import platform

    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False
    """
    While using WINDOWS use linux=False parameter, but before final contribute change in to linux=True
    :param url: input url
    :param linux: switch between linux or windows
    """
    from bs4 import BeautifulSoup
    from helpers import fetch_string

    if _OS_LINUX:
        return BeautifulSoup(fetch_string(url, cache_hours=6))
    else:
        from urllib2 import urlopen

        try:
            return BeautifulSoup(urlopen(url, timeout=20).read())
        except Exception, e:
            print e
            pass

import helpers

_host = 'http://www.assemblee-nationale.bj/'

URLS = [
    '{host}fr/deputes/listes-des-deputes'.format(host=_host)
]

POL_POS = 'political_position'
PER_NAME = 'person_name'
PIC_URL = 'picture_url'
PERSON_URL = 'url'

def _get_img_url(person_url):
    profile_page = custom_opener(person_url)
    image_link = profile_page.find('div', {'class': 'cb_field'}).find('img').get('src')
    return image_link


def _format_name(name_text, big_name):
    if big_name in name_text:
        name_with_space = big_name
        name_text = name_text.replace(big_name, '')
        return name_text + ' ' + name_with_space


def get_all_persons(url):
    page = custom_opener(url)

    all_rows = page. \
        find('table', {'class': 'cbUserListTable cbUserListT_4', 'id': 'cbUserTable'}). \
        find('tbody'). \
        find_all('tr')

    for row in all_rows:
        name, position = row.find_all('td')

        big_name = name.find('a').text
        name_text = name.text

        per_name = _format_name(name_text=name_text, big_name=big_name)
        per_url = name.find('a').get('href')
        pol_pos = position.text
        image = _get_img_url(per_url)

        obj = {
            PER_NAME: per_name,
            PERSON_URL: per_url,
            POL_POS: pol_pos,
            PIC_URL: image
        }

        yield obj

def get_entities(persons):
    for person in persons:
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]
        yield create_entity(unique_id, 'person', name, fields)


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            # helpers.check(entity)
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
