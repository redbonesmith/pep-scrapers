# /usr/bin/python -tt
# coding=utf-8
# coding=utf-8
# /usr/bin/python -tt

def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    from hashlib import sha224
    from re import sub

    if not isinstance(args, list):
        args = [args]
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url):
    import platform

    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False

    from bs4 import BeautifulSoup
    from helpers import fetch_string

    if _OS_LINUX:
        return BeautifulSoup(fetch_string(url, cache_hours=6, use_curl=False))
    else:
        import requests
        try:
            get = requests.get(url).text
            soup = BeautifulSoup(get)
            return soup
        except Exception, e:
            print e

import helpers


BASE_PAGE = 'http://www.parliament.gov.sg'

URLS = [
    '{page}/list-of-current-mps'.format(page=BASE_PAGE)
]

POL_REG = 'political_region'
POL_POS = 'political_position'
PER_NAME = 'person_name'
PIC_URL = 'picture_url'
PERSON_URL = 'url'


def name_cleaner(bad_name):
    try:
        return ' '.join(bad_name.text.split()[1:])
    except AttributeError:
        return ' '.join(bad_name.split()[1:])

def get_all_persons(url):
    page = custom_opener(url)
    main_table = page.find('div', {'class': 'view-content'}).find('tbody')
    rows = main_table.find_all('tr')

    for row in rows:
        name, region = row.find_all('td')[1:]
        person_url = BASE_PAGE+name.find('a').get('href')
        person_name = name.text

        person_page = custom_opener(person_url)
        person_info = person_page.find('div', {'id': 'particulars'})
        image = person_info.find('td', {'width': '200'}).find('img').get('src')
        position = str(person_info.find('td', {'width': '350'})).split('<br/>')[-1].strip().strip('</td>').strip('()')

        person = {
            PER_NAME: name_cleaner(person_name.strip()),
            POL_REG: region.text.strip(),
            PERSON_URL: person_url,
            PIC_URL: image,
            POL_POS: position
        }

        yield person


def get_entities(persons):
    def some_method(person):
        name = person.pop(PER_NAME)
        values = person.values()
        unique_id = create_id([_.encode('utf-8') for _ in values])
        fields = [{'tag': t, 'value': v} for t, v in person.items()]

        return create_entity(unique_id, 'person', name, fields)

    return [element for element in map(some_method, persons) if element if not None]


def main():
    for url in URLS:
        main_obj = get_all_persons(url)

        for entity in get_entities(main_obj):
            helpers.emit(entity)

# main scraper
if __name__ == "__main__":
    main()
