# coding=utf-8
# /usr/bin/python -tt

import logging as log
def create_entity(_id, entity_type, obj_name, fields, aka=False):
    default = {
        "_meta": {
            "id": _id,
            "entity_type": entity_type
        },
        "name": obj_name,
        "fields": fields
    }

    if aka:
        default.update({'aka': aka})

    return default


def create_id(args):
    from hashlib import sha224
    from re import sub
    from datetime import datetime as dt

    if not isinstance(args, list):
        args = [args]
        
    conc_names = ''.join([_.decode('utf-8') for _ in args])
    conc_names += str(dt.now())
    return sha224((sub("[^a-zA-Z0-9]", "", conc_names))).hexdigest()


def custom_opener(url, bs=True):
    import platform

    _OS_LINUX = True if "linux" in platform.system().lower() or 'unix' in platform.system().lower() else False

    from bs4 import BeautifulSoup
    from helpers import fetch_string

    if _OS_LINUX:
        page_to_str = fetch_string(url, cache_hours=6, use_curl=False)
        return BeautifulSoup(page_to_str) if bs else page_to_str
    else:
        import requests
        try:
            get = requests.get(url).text
            soup = BeautifulSoup(get)
            return soup
        except Exception, e:
            log.exception({'error': e})

def get_img(bs, attr=False):
    try:
        return bs.find('img', attr=attr)['src']
        
    except Exception, e:
        log.exception({'error': e, 'in': bs})